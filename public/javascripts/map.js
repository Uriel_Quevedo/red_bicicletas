const map = L.map("main_map").setView([-34.6012424, -58.3861497], 13);

L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
}).addTo(map);

fetch("api/bicicletas")
  .then((result) => result.json())
  .then(({ bicicletas }) => {
    console.log(bicicletas);
    bicicletas.forEach(({ ubicacion, id }) => {
      L.marker(ubicacion, { title: id }).addTo(map);
    });
  });
