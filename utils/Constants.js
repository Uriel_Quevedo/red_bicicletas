const ALL_BICICLETAS = [
  {
    color: "rojo",
    modelo: "urbana",
    ubicacion: [-34.6012424, -58.3861497],
  },
  {
    color: "blanca",
    modelo: "urbana",
    ubicacion: [-34.596932, -58.3808287],
  },
];

const ERROR = {
  BICICLETA: {
    NOT_FOUND: "No se encontro la Bicicleta.",
  },
};

module.exports = {
  ALL_BICICLETAS: ALL_BICICLETAS,
  ERROR: ERROR,
};
