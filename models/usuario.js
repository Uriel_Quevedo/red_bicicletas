const mongoose = require("mongoose");
const Reserva = require("./reserva");
const bcrypt = require("bcrypt");
const { NotExtended } = require("http-errors");
const Schema = mongoose.Schema;
const crypto = require("crypto");
const Token = require("../models/token");
const mailer = require("../mailer/mailer");
const uniqueValidator = require("mongoose-unique-validator");
const { Passport } = require("passport");

const saltRounds = 10;
const regExp = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

const validateEmail = (email) => regExp.test(email);

const usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, "El nombre es obligatorio"],
  },
  email: {
    type: String,
    trim: true,
    required: [true, "El email es obligatorio"],
    lowercase: true,
    unique: true,
    validate: [validateEmail, "Por favor, ingrese un email valido"],
    match: [regExp],
  },
  password: {
    type: String,
    required: [true, "El password es obligatorio"],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false,
  },
  googleId: String,
  facebookId: String,
});

usuarioSchema.plugin(uniqueValidator, {
  message: "El {PATH} ya existe con otro usuario",
});

usuarioSchema.index({ name: 1 });

usuarioSchema.pre("save", function (next) {
  if (this.isModified("password")) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});

usuarioSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = async function (biciId, desde, hasta) {
  const reserva = new Reserva({
    usuario: this._id,
    bicicleta: biciId,
    desde,
    hasta,
  });
  return await reserva.save();
};

usuarioSchema.methods.enviar_email_bienvenida = async function () {
  console.log("Enviar mensaje de bienvenida");

  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destinatation = this.email;

  console.log({ token });
  await token.save();

  const mailOptions = {
    from: "no-reply@redbicicletas.com",
    to: email_destinatation,
    subject: "Verificacion de cuenta",
    text: `Hola, por favor, para verificar su cuenta haga click en este link: http://localhost:5000/token/confirmation/${token.token}`,
  };

  await mailer.sendMail(mailOptions);
  console.log(`Se ha enviado un email de bienvenida a ${email_destinatation}`);
};

usuarioSchema.methods.resetPassword = async function () {
  console.log("Enviar mensaje de nueva contraseña");

  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destinatation = this.email;

  token.save(async function (err) {
    if (err) return cb(err);

    const mailOptions = {
      from: "no-reply@redbicicletas.com",
      to: email_destinatation,
      subject: "Verificacion de cuenta",
      text: `Hola, por favor, para verificar su cuenta haga click en este link: http://localhost:5000/token/confirmation/${token.token}`,
    };

    await mailer.sendMail(mailOptions);
    console.log(
      `Se ha enviado un email para resetear la password a ${email_destinatation}`
    );

    cb(null);
  });
};

const Usuario = mongoose.model("Usuario", usuarioSchema);
module.exports = Usuario;

Usuario.findOneOrCreateByGoogle = function findOneOrCreate(
  condition,
  callback
) {
  const self = this;
  console.log(condition);

  self.findOne(
    {
      $or: [{ googleId: condition.id }, { email: condition.emails[0].value }],
    },
    (err, result) => {
      if (result) {
        callback(err, result);
      } else {
        console.log("------------ CONDITION --------------");
        console.log(condition);
        let values = {};
        values.googleId = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || "SIN NOMBRE";
        values.verificado = true;
        values.password = crypto.randomBytes(16).toString("hex");
        console.log("------------ VALUES -----------------");
        console.log(values);
        self.create(values, (err, result) => {
          if (err) console.log(err);
          return callback(err, result);
        });
      }
    }
  );
};

Usuario.findOneOrCreateByFacebook = function findOneOrCreate(
  condition,
  callback
) {
  const self = this;
  console.log(condition);

  self.findOne(
    {
      $or: [{ facebookId: condition.id }, { email: condition.emails[0].value }],
    },
    (err, result) => {
      if (result) {
        callback(err, result);
      } else {
        console.log("------------ CONDITION --------------");
        console.log(condition);
        let values = {};
        values.facebookId = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || "SIN NOMBRE";
        values.verificado = true;
        values.password = condition._json.sub;
        console.log("------------ VALUES -----------------");
        console.log(values);
        self.create(values, (err, result) => {
          if (err) console.log(err);
          return callback(err, result);
        });
      }
    }
  );
};

Usuario.generate = async (nombre) => {
  const newUsuario = new Usuario({ nombre });
  return await newUsuario.save();
};
