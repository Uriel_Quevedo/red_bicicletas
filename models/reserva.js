const mongoose = require("mongoose");
const moment = require("moment");
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

const reservaSchema = new Schema({
  desde: Date,
  hasta: Date,
  bicicleta: { type: ObjectId, ref: "Bicicleta" },
  usuario: { type: ObjectId, ref: "Usuario" },
});

reservaSchema.methods.diasDeReserva = function () {
  return moment(this.hasta).diff(moment(this.desde), "days") + 1;
};

module.exports = mongoose.model("Reserva", reservaSchema);
