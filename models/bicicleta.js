const Constants = require("../utils/Constants");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: { type: "2dsphere", sparse: true },
  },
});

bicicletaSchema.index({ code: 1 });

bicicletaSchema.methods.toString = () => {
  return `id: ${this.id} | color: ${this.color}`;
};

const Bicicleta = mongoose.model("Bicicleta", bicicletaSchema);
module.exports = Bicicleta;

Bicicleta.createInstance = (newBicicleta) => {
  return new Bicicleta(newBicicleta);
};

Bicicleta.allBicis = async () => {
  return await Bicicleta.find({});
};

Bicicleta.generate = async (aBici) => {
  const newBici = new Bicicleta(aBici);
  return await newBici.save();
};

Bicicleta.findOneByCode = async (code) => {
  return await Bicicleta.findOne({ code });
};

Bicicleta.removeByCode = async (code) => {
  return await Bicicleta.deleteOne({ code });
};
