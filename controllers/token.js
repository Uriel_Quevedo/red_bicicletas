const Usuario = require("../models/usuario");
const Token = require("../models/token");

module.exports = {
  confirmationGet: async (req, res) => {
    const token = await Token.findOne({ token: req.params.token });
    if (!token)
      return res.status(400).send({
        type: "not-verified",
        msg: "No se encontro un usuario con ese token",
      });

    const usuario = await Usuario.findById(token._userId);
    if (!usuario)
      return (
        res, status(400).send({ msg: "No se encontro un usario con ese token" })
      );

    if (usuario.verificado) return res.redirect("/usuarios");
    usuario.verificado = true;
    await usuario.save();

    res.redirect("/");
  },
};
