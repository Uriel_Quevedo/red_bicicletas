const Usuario = require("../models/usuario");

module.exports = {
  list: async (req, res) => {
    const usuarios = await Usuario.find({});
    res.render("usuarios/index", { usuarios });
  },

  update_get: async (req, res) => {
    const usuario = await Usuario.findById(req.params.id);
    res.render("usuarios/update", { errors: {}, usuario });
  },

  update: async (req, res) => {
    const { nombre, email } = req.body;
    const update_values = { nombre };
    try {
      const usuario = await Usuario.findByIdAndUpdate(
        req.params.id,
        update_values
      );
      res.redirect("/usuarios");
    } catch (error) {
      console.log(error);
      res.render("usuarios/update", {
        errors: error.errors,
        usuarios: new Usuario({ nombre, email }),
      });
    }
  },

  create_get: async (req, res) => {
    res.render("usuarios/create", { errors: {}, usuario: new Usuario({}) });
  },

  create: async (req, res) => {
    const { password, confirm_password, nombre, email } = req.body;
    if (password !== confirm_password) {
      res.render("usuarios/create", {
        errors: {
          confirm_password: { message: "Las contraseñas no coinciden" },
        },
        usuario: new Usuario({ nombre, email }),
      });
      return;
    }

    try {
      const usuario = await Usuario.create({ nombre, email, password });
      usuario.enviar_email_bienvenida();
      res.redirect("/usuarios");
    } catch (error) {
      res.render("usuarios/create", {
        errors: error.errors,
        usuario: new Usuario({ nombre, email }),
      });
    }
  },

  delete: async (req, res, next) => {
    try {
      await Usuario.findByIdAndDelete(req.body.id);
      res.redirect("/usuarios");
    } catch (error) {
      next(error);
    }
  },
};
