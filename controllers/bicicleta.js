const Bicicleta = require("../models/bicicleta");

exports.allBicicletas = function (req, res) {
  res.render("bicicletas/index", { data: Bicicleta.allBicis });
};

exports.bicicleta_create_get = function (req, res) {
  res.render("bicicletas/create");
};

exports.bicicleta_create_post = function (req, res) {
  const { lat, lng, ...rest } = req.body;
  const bici = new Bicicleta({ ...rest, ubicacion: [lat, lng] });
  Bicicleta.add(bici);

  res.redirect("/bicicletas");
};

exports.bicicleta_delete_post = function (req, res) {
  Bicicleta.removeById(req.body.id);

  res.redirect("/bicicletas");
};

exports.bicicleta_update_get = function (req, res) {
  const bici = Bicicleta.findById(req.params.id);

  res.render("bicicletas/update", { bici });
};

exports.bicicleta_update_post = function (req, res) {
  const bici = Bicicleta.findById(req.params.id);
  const { lat, lng, color, modelo, id } = req.body;
  const ubicacion = [lat, lng];

  bici.ubicacion = ubicacion;
  bici.color = color;
  bici.modelo = modelo;
  bici.id = id;

  res.redirect("/bicicletas");
};
