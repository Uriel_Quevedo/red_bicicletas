const Bicicleta = require("../../models/bicicleta");

exports.bicicleta_list = async function (req, res) {
  const bicicletas = await Bicicleta.allBicis();
  res.status(200).json({
    bicicletas,
  });
};

exports.bicicleta_create = async function (req, res) {
  const { lat, lng, ...rest } = req.body;
  const bicicleta = await Bicicleta.generate({
    ...rest,
    ubicacion: [lat, lng],
  });

  res.status(200).json({
    bicicleta,
  });
};

exports.bicicleta_delete = function (req, res) {
  Bicicleta.removeById(req.body.id);

  res.status(204).send();
};

exports.bicicleta_update = function (req, res) {
  const bici = Bicicleta.findById(req.params.id);
  const { lat, lng, color, modelo, id } = req.body;
  const ubicacion = [lat, lng];

  bici.ubicacion = ubicacion;
  bici.color = color;
  bici.modelo = modelo;
  bici.id = id;

  res.status(200).json({
    bicicleta: bici,
  });
};
