const Usuario = require("../../models/usuario");

exports.usuarios_list = async (req, res) => {
  const usuarios = await Usuario.find({});
  res.status(200).json({ usuarios });
};

exports.usuarios_create = async (req, res) => {
  const { nombre } = req.body;
  const usuario = await Usuario.generate(nombre);

  res.status(200).json({ usuario });
};

exports.usuarios_reservar = async (req, res) => {
  const { id, bici_id, desde, hasta } = req.body;
  const usuario = await Usuario.findById(id);
  await usuario.reservar(bici_id, desde, hasta);
  res.status(200).send();
};
