require("dotenv").config();
var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var bicicletasRouter = require("./routes/bicicletas");
var bicicletasRouterApi = require("./routes/api/bicicletas");
var usuariosRouterApi = require("./routes/api/usuarios");
var usuariosRouter = require("./routes/usuarios");
var tokenRouter = require("./routes/token");
var authRouterApi = require("./routes/api/auth");
const passport = require("./config/passport");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
const Usuario = require("./models/usuario");
const jwt = require("jsonwebtoken");

let store;
if (process.env.NODE_ENV === "development") {
  store = new session.MemoryStore();
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: "sessions",
  });
  store.on("error", function (error) {
    assert.ifError(error);
    assert.ok(false);
  });
}

var app = express();

app.set("secretKey", "jwt_pwd_!!223344");

app.use(
  session({
    cookie: { maxAge: 240 + 60 * 60 * 1000 },
    store: store,
    saveUninitialized: true,
    resave: "true",
    secret: 'red_bicis_!!***!"-!"-!"-!"-!"-!"-123123',
  })
);

var mongoose = require("mongoose");
const { assert } = require("console");

var mongoDB = process.env.MONGO_URI;

mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error"));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, "public")));

app.get("/login", function (req, res) {
  res.render("session/login");
});

app.post("/login", function (req, res, next) {
  passport.authenticate("local", function (err, usuario, info) {
    console.log({ usuario, info });
    if (err) return next(err);
    if (!usuario) return res.render("session/login", { info });
    req.logIn(usuario, function (err) {
      if (err) return next(err);
      return res.redirect("/");
    });
  })(req, res, next);
});

app.get("/logout", function (req, res) {
  req.logOut();
  res.redirect("/");
});

app.get("/forgotPassword", function (req, res) {
  res.render("session/forgotPassword");
});

app.post("/forgotPassword", function (req, res) {
  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    if (!usuario)
      return res.render("session/forgotPassword", {
        info: { message: "No existe el email" },
      });

    usuario.resetPassword(function (err) {
      if (err) return next(err);
      console.log("session/forgotPasswordMessage");
    });
    res.render("session/forgotPasswordMessage");
  });
});

app.get("/resetPassword/:token", function (req, res) {
  Usuario.findOne({ token: req.params.token }, function (err, token) {
    if (!token)
      return res
        .status(400)
        .send({ type: "not-verified", msg: "No existe el usuario" });

    Usuario.findById(token._userId, function (err, usuario) {
      if (!usuario)
        return res.status(400).send({ msg: "No existe un usuario" });
      res.render("session/resetPassword", { errors: {}, usuario });
    });
  });
});

app.post("/resetPassword", function (req, res) {
  if (req.body.password != req.body.confirm_password) {
    res.render("session/resetPaswword", {
      errors: { confirm_password: { message: "No coinciden las contraseñas" } },
    });
    return;
  }

  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    usuario.password = req.body.password;
    usuario.save(function (error) {
      if (err) {
        res.render("session/resetPassword", {
          erros: err.errors,
          usuario: new Usuario({ email: req.body.email }),
        });
      } else {
        res.redirect("(login");
      }
    });
  });
});

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/bicicletas", loggedIn, bicicletasRouter);
app.use("/usuarios", usuariosRouter);
app.use("/token", tokenRouter);

app.use("/api/auth", authRouterApi);
app.use("/api/bicicletas", validarUsuario, bicicletasRouterApi);
app.use("/api/usuarios", usuariosRouterApi);

app.use("/privacy_policy", function (req, res) {
  res.sendFile("public/policy_privacy.html");
});

app.use("/googled2ac96c89df94930", function (req, res) {
  res.sendFile("public/googled2ac96c89df94930.html");
});

app.get(
  "/auth/google",
  passport.authenticate("google", { scope: ["email", "profile"] })
);

app.get(
  "/auth/google/callback",
  passport.authenticate("google", {
    successRedirect: "/",
    failureRedirect: "/error",
  })
);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log("user sin loguearse");
    res.redirect("login");
  }
}

function validarUsuario(req, res, next) {
  jwt.verify(
    req.headers["x-access-token"],
    req.app.get("secretKey"),
    function (err, decoded) {
      if (err) {
        res.json({ status: "error", message: err.message, data: null });
      } else {
        req.body.userId = decoded.id;
        console.log("jwt verifit: " + decoded);
        next();
      }
    }
  );
}

module.exports = app;
