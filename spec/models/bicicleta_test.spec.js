const Bicicleta = require("../../models/bicicleta");
const Constants = require("../../utils/Constants");
const mongoose = require("mongoose");

describe("Testing Bicicletas", () => {
  beforeEach((done) => {
    const mongoDB = "mongodb://127.0.0.1:27018/testdb";
    mongoose.connect(mongoDB, { useNewUrlParser: true });

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", () => {
      console.log("We are connected to test databse");
      done();
    });
    done();
  });

  afterEach(async (done) => {
    try {
      await Bicicleta.deleteMany({});
    } catch (error) {
      console.log(error);
    }
    done();
  });

  describe("Bicicleta.createInstance", () => {
    it("Crea una instancia de Bicicleta", () => {
      const aBici = Bicicleta.createInstance({
        ...Constants.ALL_BICICLETAS[0],
        code: 1,
      });

      expect(aBici.code).toBe(1);
    });
  });

  describe("Bicicleta.allBicis", () => {
    it("Se obtiene las bicis", async (done) => {
      const bicis = await Bicicleta.allBicis();
      expect(bicis.length).toBe(0);
      done();
    });
  });

  describe("Bicicleta.add", () => {
    it("Agrega una sola bici", async (done) => {
      const aBici = await Bicicleta.generate({
        ...Constants.ALL_BICICLETAS[0],
        code: 1,
      });

      const bicis = await Bicicleta.allBicis();

      expect(bicis.length).toBe(1);
      expect(bicis[0].code).toBe(aBici.code);
      done();
    });
  });

  describe("Bicicleta.findOneByCode", () => {
    it("Obtengo una bici por su code", async (done) => {
      const bicis = Constants.ALL_BICICLETAS;
      let code = 0;
      for (const aBici in bicis) {
        code++;
        await Bicicleta.generate({ ...aBici, code });
      }

      const bici_found_code_1 = await Bicicleta.findOneByCode(1);
      const bici_found_code_2 = await Bicicleta.findOneByCode(2);
      const allBicis = await Bicicleta.allBicis();

      expect(allBicis.length).toBe(2);
      expect(bici_found_code_1.code).toBe(1);
      expect(bici_found_code_2.code).toBe(2);
      done();
    });
  });
});
