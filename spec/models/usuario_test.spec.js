const mongoose = require("mongoose");
const Bicicleta = require("../../models/bicicleta");
const Usuario = require("../../models/usuario");
const Reserva = require("../../models/reserva");
const reserva = require("../../models/reserva");

describe("Testing Usuarios", () => {
  beforeEach((done) => {
    const mongoDB = "mongodb://127.0.0.1:27018/testdb";
    mongoose.connect(mongoDB, { useNewUrlParser: true });

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", () => {
      console.log("We are connected to test databse");
      done();
    });
    done();
  });

  afterEach(async (done) => {
    try {
      await Reserva.deleteMany({});
      await Usuario.deleteMany({});
      await Bicicleta.deleteMany({});
    } catch (error) {
      console.log(error);
    }
    done();
  });

  describe("Cuando un usuario reserva una Bici", () => {
    it("debe existir la reserva", async (done) => {
      const usuario = await Usuario.generate("Uriel");

      const bicicleta = await Bicicleta.generate({
        code: 1,
        color: "Verde",
        modelo: "urbana",
      });

      const hoy = new Date();
      const mañana = new Date();
      mañana.setDate(hoy.getDate() + 1);

      await usuario.reservar(bicicleta._id, hoy, mañana);
      const reservas = await Reserva.find({})
        .populate("bicicleta")
        .populate("usuario");

      expect(reservas.length).toBe(1);
      expect(reservas[0].diasDeReserva()).toBe(2);
      expect(reservas[0].bicicleta.code).toBe(1);
      expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
      done();
    });
  });
});
