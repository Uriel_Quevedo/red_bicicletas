const Bicicleta = require("../../models/bicicleta");
const request = require("request");
const Constants = require("../../utils/Constants");
const mongoose = require("mongoose");

const server = require("../../bin/www");

const BASE_URL = "http://localhost:3000/api/bicicletas";

const HEADERS = { "content-type": "application/json" };

describe("Bicicleta API", () => {
  beforeAll(async () => {
    await mongoose.disconnect();
  });

  beforeAll((done) => {
    const mongoDB = "mongodb://127.0.0.1:27018/testdb";
    mongoose.connect(mongoDB, { useNewUrlParser: true });

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", () => {
      console.log("We are connected to test databse");
      done();
    });
    done();
  });

  afterEach(async (done) => {
    try {
      await Bicicleta.deleteMany({});
    } catch (error) {
      console.log(error);
    }
    done();
  });

  describe("GET BICICLETAS /", () => {
    it("status 200", async (done) => {
      const bicis = await Bicicleta.allBicis();
      expect(bicis.length).toBe(0);

      await Bicicleta.generate({ ...Constants.ALL_BICICLETAS[0], id: 0 });

      request.get(BASE_URL, (err, res, body) => {
        expect(res.statusCode).toBe(200);
      });
      done();
    });
  });

  describe("POST BICICLETRAS /create", () => {
    it("status 200", (done) => {
      const aBici =
        '{ "code": 10, "color": "rojo", "modelo": "urbana", "lat": -31, "lng": -34}';

      request.post(
        {
          headers: HEADERS,
          url: BASE_URL,
          body: aBici,
        },
        async (err, res, body) => {
          const bici = await Bicicleta.findOneByCode(10);
          expect(res.statusCode).toBe(200);
          expect(bici.code).toBe(10);
          done();
        }
      );
    });
  });
});
