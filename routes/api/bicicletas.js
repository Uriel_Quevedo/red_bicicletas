const express = require("express");
const router = express.Router();
const bicicletaController = require("../../controllers/api/bicicletaControllerAPI");

router.get("/", bicicletaController.bicicleta_list);
router.post("/", bicicletaController.bicicleta_create);
router.delete("/", bicicletaController.bicicleta_delete);
router.put("/:id", bicicletaController.bicicleta_update);

module.exports = router;
